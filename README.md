**#prerequisite**

1. Amazon Developer Account --> developer.amazon.com
2. Openweathermap Account --> https://openweathermap.org/

**API URL**
--> http://api.openweathermap.org/data/2.5/weather?appid={YOUR_API_KEY}&q={CITY_NAME}&units=metric

**Video to generate API key**
--> https://www.youtube.com/watch?v=oN4ohLj0fkY

For units=metric --> celsius and units=imperial --> fahrenheit
